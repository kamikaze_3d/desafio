## Como iniciar a aplicação
- Instale o [npm](https://nodejs.org/en/download/)
- clone ou faça o download deste repositório no seu computador
- entre no diretorio raiz do projeto 
- navegue com o terminal ate a pasta /frontend
- Inicie o projeto
```
npm criar
```
- aguarde o termino da instalação
- Acesse o site em http://localhost:8888

## Como enviar seu teste
- Envie um email para [carreira@webjump.com.br] com o link do seu repositório

