import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";

import Header from "../components/shared/Header";
import Footer from "../components/shared/Footer";
import Mobile from "../components/shared/MenuMobile";
import Search from "../components/shared/SeachModal";
import Filters from "../components/shared/listFilter";

import Home from "../pages/Home";
import Camisetas from "../pages/Camisetas";
import Calcas from "../pages/Calcas";
import Calcados from "../pages/Calcados";

// const api = () => (

// );

export default class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuLinks: "",
      camisetas: "",
      calcados: "",
      calcas: ""
    };
  }

  componentDidMount() {
    const url = "http://localhost:8888/api/V1/categories";

    fetch(url + "/list")
      .then(body => body.json())
      .then(res => {
        this.setState({ menuLinks: res.items });
      });
    fetch(url + "/1")
      .then(body => body.json())
      .then(res => {
        this.setState({ camisetas: res });
      });
    fetch(url + "/2")
      .then(body => body.json())
      .then(res => {
        this.setState({ calcas: res });
      });
    fetch(url + "/3")
      .then(body => body.json())
      .then(res => {
        this.setState({ calcados: res });
      });
  }

  render() {
    return (
      <BrowserRouter>
        <Header links={this.state.menuLinks} />
        <section className="uk-section">
          <div className="uk-container">
            <div className="uk-grid-small" uk-grid="">
              <Route
                path="/camisetas"
                component={() => (
                  <Camisetas
                    dados={this.state.camisetas}
                    links={this.state.menuLinks}
                    tipo={Filters}
                  />
                )}
              />
              <Route
                path="/"
                exact
                component={() => (
                  <Home
                    camiseta={this.state.camisetas}
                    calcas={this.state.calcas}
                    calcados={this.state.calcados}
                    links={this.state.menuLinks}
                    tipo={Filters}
                  />
                )}
              />
              <Route
                path="/calcas"
                component={() => (
                  <Calcas
                    dados={this.state.calcas}
                    links={this.state.menuLinks}
                    tipo={Filters}
                  />
                )}
              />
              <Route
                path="/calcados"
                component={() => (
                  <Calcados
                    dados={this.state.calcados}
                    links={this.state.menuLinks}
                    tipo={Filters}
                  />
                )}
              />
            </div>
          </div>
        </section>
        <Footer />
        <Mobile links={this.state.menuLinks} />
        <Search />
      </BrowserRouter>
    );
  }
}
