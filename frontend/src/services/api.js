const url = "http://localhost:8888/api/V1/categories/list";

var baseHeader = {
  method: "GET",
  headers: {
    "Content-Type": "text/plain",
    "Access-Control-Allow-Origin": "*",
    "X-Custom-Header": "ProcessThisImmediately"
  },
  mode: "cors",
  cache: "default"
};

var request = new Request(url, baseHeader);

fetch(request)
  .then(res => res.json)
  .then(data => console.log(data));
