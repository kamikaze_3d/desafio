import React from "react";

export default class HeaderProduct extends React.Component {
  constructor() {
    super();
    this.state = {
      item_select: ""
    };
  }

  ordena = e => {
    this.props.reordem(e.target.value);
  };

  render() {
    const { titulo, ordem } = this.props;
    return (
      <div className="uk-padding-remove uk-margin-bottom uk-width-1-1">
        <div className="uk-margin-left uk-margin-right">
          <h1 className="uk-margin-small-bottom uk-text-primary">{titulo}</h1>
          <hr className="uk-margin-remove" />
          <div className="uk-flex uk-flex-middle uk-flex-between">
            <div className="uk-width-1-2 uk-flex uk-flex-left uk-flex-middle">
              <a href="/#" className="card uk-margin-right">
                card
              </a>
              <a href="/#" className="list">
                lista
              </a>
            </div>
            <div className="uk-width-1-2 uk-flex uk-flex-middle">
              <div className="uk-margin-remove uk-text-small uk-text-bold uk-text-uppercase uk-width-1-2 uk-text-right uk-padding-small-right">
                Ordenar por
              </div>
              <select
                onChange={this.ordena.bind(this)}
                className="uk-select uk-margin-left uk-width-1-2"
              >
                {ordem.map(res => (
                  <option key={res.id} value={res.valor}>
                    {res.titulo}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <hr className="uk-margin-remove" />
        </div>
      </div>
    );
  }
}
