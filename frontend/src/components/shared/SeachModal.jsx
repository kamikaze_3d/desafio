import React from "react";

const SearchModal = () => (
  <div id="modal-full" className="uk-modal-full uk-modal" uk-modal="">
    <div
      className="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle"
      uk-height-viewport=""
    >
      <button
        className="uk-modal-close-full"
        type="button"
        uk-close=""
      ></button>
      <form className="uk-search uk-search-large">
        <input
          className="uk-search-input uk-text-center"
          type="search"
          placeholder="Buscar..."
        />
      </form>
    </div>
  </div>
);

export default SearchModal;
