import React from "react";
import { Link } from "react-router-dom";

const MobileMenu = ({ links }) => (
  <div id="mobile-menu" uk-offcanvas="overlay: true; mode: push">
    <div className="uk-offcanvas-bar">
      <button
        className="uk-offcanvas-close uk-close-large uk-icon uk-close"
        type="button"
        uk-close=""
      ></button>
      <div className="uk-height-1-1 uk-flex uk-flex-middle uk-flex-center">
        <ul
          className="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical uk-nav-parent-icon"
          uk-nav=""
        >
          {links
            ? links.map(res => (
                <li key={res.id}>
                  <Link to={res.path}>{res.name}</Link>
                </li>
              ))
            : "Carregando..."}
        </ul>
      </div>
    </div>
  </div>
);

export default MobileMenu;
