import React from "react";
import { Link } from "react-router-dom";

const Menu = ({ dados }) => (
  <section className="uk-section-primary">
    <div className="uk-container">
      <ul className="uk-navbar-nav wj-menu">
        {dados
          ? dados.map(res => (
              <li key={res.id}>
                <Link to={res.path}>{res.name}</Link>
              </li>
            ))
          : "Carregando..."}
      </ul>
    </div>
  </section>
);

export default Menu;
