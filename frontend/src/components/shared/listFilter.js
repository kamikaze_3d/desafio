export default {
  genero: [
    { id: 1, titulo: "Masculina", valor: "Masculina" },
    { id: 2, titulo: "Feminina", valor: "Feminina" },
    { id: 3, titulo: "-- todos --", valor: "" }
  ],
  corCamisa: [
    { id: 1, valor: "Preta", titulo: "black" },
    { id: 2, valor: "Laranja", titulo: "orange" },
    { id: 3, valor: "Amarela", titulo: "yellow" },
    { id: 4, valor: "Rosa", titulo: "pink" },
    { id: 5, valor: "", titulo: "--" }
  ],
  corCalcado: [
    { id: 1, valor: "Preta", titulo: "black" },
    { id: 2, valor: "Laranja", titulo: "orange" },
    { id: 3, valor: "Amarela", titulo: "yellow" },
    { id: 4, valor: "Rosa", titulo: "pink" },
    { id: 5, valor: "Cinza", titulo: "gray" },
    { id: 6, valor: "Azul", titulo: "blue" },
    { id: 7, valor: "", titulo: "--" }
  ],
  ordem: [
    { id: 1, titulo: "--", valor: "" },
    { id: 2, titulo: "Nome", valor: "name" },
    { id: 3, titulo: "Preço", valor: "price" }
  ]
};
