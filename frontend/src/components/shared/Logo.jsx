import React from "react";

const Logo = ({ display }) => (
  <div className="uk-logo">
    <img className={display} src={require("../assets/logo.png")} alt="" />
  </div>
);

export default Logo;
