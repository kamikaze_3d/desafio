import React from "react";
import { Link } from "react-router-dom";

export default class Sidebar extends React.Component {
  constructor() {
    super();
    this.state = {
      selecionado: ""
    };
  }

  disparaAcao(e) {
    this.props.filtro(e.target.value);
  }

  disparaAcao2(e) {
    this.props.filtro(e.target.value);
  }

  render() {
    const { categorias, tipo, titulo } = this.props;
    return (
      <div className="uk-padding-small uk-margin-right uk-margin-remove-right@s wj-sidebar">
        <h3 className="uk-text-primary uk-margin-small uk-text-uppercase uk-text-bold">
          Filtre por
        </h3>
        <div>
          <h4 className="uk-text-muted uk-text-uppercase uk-text-bold">
            Categorias
          </h4>
          <ul className="uk-list uk-list-bullet">
            {categorias
              ? categorias.map(res => (
                  <li key={res.id}>
                    <Link to={res.path}>{res.name}</Link>
                  </li>
                ))
              : "carregando..."}
          </ul>

          <h4 className="uk-text-muted uk-text-uppercase uk-text-bold">
            {titulo}
          </h4>
          {titulo == "Gênero" ? (
            <div className="uk-list uk-list-bullet">
              {tipo
                ? tipo.map(item => (
                    <p key={item.id}>
                      <label>
                        <input
                          onChange={this.disparaAcao.bind(this)}
                          value={item.valor}
                          name="sidebar1"
                          className="uk-radio  uk-margin-small-right uk-margin-small-left"
                          type="radio"
                        />
                        {item.titulo}
                      </label>
                    </p>
                  ))
                : ""}
            </div>
          ) : (
            <div className="uk-child-width-1-3 wj-cor-paleta">
              {tipo
                ? tipo.map(item => (
                    <label key={item.id}>
                      <input
                        onChange={this.disparaAcao2.bind(this)}
                        value={item.valor}
                        className="uk-button wj-cor"
                        style={{
                          backgroundColor: item.titulo,
                          color: item.titulo
                        }}
                        className="uk-radio"
                        type="radio"
                        name="radio2"
                      />
                    </label>
                  ))
                : ""}
            </div>
          )}
          {/* <h4 className="uk-text-muted uk-text-uppercase uk-text-bold">Cor</h4>
      <div className="uk-child-width-1-3">
        <a className="uk-button wj-cor-1" href="">
          .
        </a>
        <a className="uk-button wj-cor-2" href="">
          .
        </a>
        <a className="uk-button wj-cor-3" href="">
          .
        </a>
      </div>
      <h4 className="uk-text-muted uk-text-uppercase uk-text-bold">Tipo</h4>
      <ul className="uk-list uk-list-bullet">
        <li>
          <Link to="/s">Corrida</Link>
        </li>
        <li>
          <Link to="/s">Caminhada</Link>
        </li>
        <li>
          <Link to="/s">Casual</Link>
        </li>
        <li>
          <Link to="/s">Social</Link>
        </li>
      </ul> */}
        </div>
      </div>
    );
  }
}
