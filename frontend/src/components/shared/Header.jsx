import React from "react";
import LoginBar from "./LoginBar";
import Logo from "./Logo";
import Menu from "./Menu";
import Search from "./Search";

const Header = ({ links }) => (
  <section uk-sticky="cls-active: mobile uk-navbar-sticky uk-background-default;  top:300">
    <LoginBar />
    <header className="uk-section-small uk-box-shadow-small">
      <div className="uk-container">
        <nav uk-navbar="">
          <div className="uk-navbar-left">
            <div className="uk-visible@m">
              <Logo display="desktop" />
            </div>
            <a
              href="#mobile-menu"
              uk-toggle=""
              className=" uk-hidden@m"
              uk-icon="icon: menu; ratio: 1.5"
            ></a>
          </div>

          <div className="uk-navbar-center uk-hidden@m">
            <Logo display="mobile" />
          </div>

          <div className="uk-navbar-right">
            <div className="uk-visible@m">
              <Search />
            </div>
            <div className="uk-hidden@m">
              <a
                href="#modal-full"
                className=" uk-hidden@m uk-text-primary"
                uk-icon="icon: search; ratio: 1.5"
                uk-toggle=""
              ></a>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <div className="uk-visible@m">
      <Menu dados={links} />
    </div>
  </section>
);
export default Header;

// vermelho #cc0d1f
// verde #80bdb8
// preto #231f20
