import React from "react";

const Search = () => (
  <form className="uk-search uk-search-default uk-width-1-1 wj-pesquisa">
    <div className="uk-flex">
      <input className="uk-search-input uk-width-large" type="search" />
      <button className="uk-button uk-button-primary" type="submit">
        Buscar
      </button>
    </div>
  </form>
);

export default Search;
