import React from "react";

const LoginBar = () => (
  <section className="wj-login uk-background-secondary">
    <div className="uk-container">
      <div className="uk-flex uk-flex-right">
        <a href="asda">Acesse sua Conta </a>
        <span> ou </span>
        <a href="asd"> Cadastre-se</a>
      </div>
    </div>
  </section>
);

export default LoginBar;
