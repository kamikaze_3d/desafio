import React from "react";

const CardProduto = ({ dados }) => (
  <div className="uk-card uk-card-body uk-card-small wj-card">
    <div className="uk-card-media-top uk-flex uk-height-small uk-flex-center wj-card-img">
      <img src={dados.image} alt="" />
    </div>
    <div className="uk-margin-small-top">
      <h3 className="uk-card-title uk-flex uk-flex-center uk-flex-middle uk-margin-remove wj-card-title">
        {dados.name}
      </h3>
      {dados.specialPrice ? (
        <>
          <p className="uk-text-center uk-margin-small uk-margin-remove wj-card-valor-antigo">
            <del> R$ {dados.price.toString().replace(".", ",")} </del>
          </p>
          <p className="uk-text-center uk-text-primary uk-text-bold uk-margin-small uk-margin-remove-top wj-card-valor">
            R$ {dados.specialPrice.toString().replace(".", ",")}
          </p>
        </>
      ) : (
        <p className="uk-text-center uk-text-primary uk-text-bold uk-margin-small wj-card-valor">
          R$ {dados.price.toString().replace(".", ",")}
        </p>
      )}
    </div>
    <div>
      <a
        href="#"
        className="uk-button uk-button-default uk-width-1-1 wj-card-btn"
      >
        Comprar
      </a>
    </div>
  </div>
);

export default CardProduto;
