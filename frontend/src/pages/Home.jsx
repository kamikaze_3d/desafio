import React from "react";
import Produto from "../components/produto/Card";
import HeaderProduct from "../components/shared/HeaderProduct";
import Sidebar from "../components/shared/Sidebar";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valor_filtro: "",
      ordem: ""
    };
  }
  filtraProdutos(e) {
    this.setState({
      valor_filtro: e
    });
  }
  ordenaProdutos(e) {
    this.setState({
      ordem: e
    });
  }
  render() {
    const { camiseta, calcas, calcados, links, tipo } = this.props;
    return (
      <>
        <div className="uk-width-1-4@m uk-margin-bottom">
          <Sidebar
            filtro={this.filtraProdutos.bind(this)}
            titulo="Gênero"
            categorias={links}
            tipo={tipo.genero}
          />
        </div>
        <main
          className="uk-width-3-4@m uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-margin-small-left  uk-margin-remove-left@m "
          uk-grid=""
        >
          <HeaderProduct
            titulo="Produtos"
            reordem={this.ordenaProdutos.bind(this)}
            ordem={tipo.ordem}
          />
          {camiseta.items
            ? camiseta.items

                .filter(res => {
                  var filtro = this.state.valor_filtro;
                  if (filtro !== "") {
                    return res.filter[0].gender === this.state.valor_filtro;
                  } else {
                    return res;
                  }
                })
                .map(res => <Produto key={res.id} dados={res} />)
            : "Carregando..."}
          {calcas.items
            ? calcas.items

                .filter(res => {
                  var filtro = this.state.valor_filtro;
                  if (filtro !== "") {
                    return res.filter[0].gender === this.state.valor_filtro;
                  } else {
                    return res;
                  }
                })
                .map(res => <Produto key={res.id} dados={res} />)
            : "Carregando..."}
          {calcados.items
            ? calcados.items

                .filter(res => {
                  var filtro = this.state.valor_filtro;
                  if (filtro !== "") {
                    return res.filter[0].gender === this.state.valor_filtro;
                  } else {
                    return res;
                  }
                })
                .map(res => <Produto key={res.id} dados={res} />)
            : "Carregando..."}
        </main>
      </>
    );
  }
}
