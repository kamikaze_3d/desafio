import React from "react";
import "./sass/styles.sass";

import Routes from "./services/router";

const App = () => <Routes />;

export default App;
